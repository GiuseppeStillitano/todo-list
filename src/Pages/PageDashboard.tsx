import React, { useContext } from 'react'
import NewTask from '../components/Containers/NewTask/NewTask'
import { TaskBox } from '../components/Containers/TaskBox/TaskBox'
import { TemplatePages } from '../components/Containers/TemplatePages/TemplatePages'
import { Counter, TitleH4 } from '../components/UI'
import { GlobalContext } from '../hooks/context/Context'

export default function Dashboard() {
    const context = useContext(GlobalContext)
    return (
        <TemplatePages>
            {renderSectionNewTask()}
            {renderSectionInProgress()}
            {renderSectionDone()}
        </TemplatePages>
    )

    function renderSectionNewTask(){
        return (
            <>
                <TitleH4 marginTop={'40px'} hideMobile>New Task</TitleH4>
                <NewTask/>
            </>
        );
    }

    function renderSectionInProgress(){
        const { tasks } = context!.todo.state;
        const countTask = tasks.filter(x => x.isCompleted !== true).length
        return (
            <>
                <TitleH4 marginTop={'40px'}> In progress
                    <Counter secondary marginLeft='20px'>{countTask}</Counter>
                    {renderBox({isCompleted: false})}
                </TitleH4>
            </>
        )
    }

    function renderSectionDone(){
        const { tasks } = context!.todo.state;
        const countTask = tasks.filter(x => x.isCompleted === true).length
        return (
            <>
                <TitleH4 marginTop={'40px'}> Done
                    <Counter secondary marginLeft='20px'>{countTask}</Counter>
                </TitleH4>
                {renderBox({isCompleted: true})}
            </>
        )
    }

    function renderBox({isCompleted}: {isCompleted: boolean}) {
        const { tasks } = context!.todo.state
        return tasks
            .sort(a => (a.isPriority === true) ? -1 : 0)
            .filter(x => x.isCompleted === isCompleted)
            .map(x => {
                return (
                    <TaskBox
                        key={x.id}
                        id={x.id}
                        content={x.content}
                        isPriority={x.isPriority}
                        isCompleted={x.isCompleted}/>
                )
        })
    }
}
