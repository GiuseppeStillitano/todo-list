import { DefaultTheme, ThemeProvider } from "styled-components"

interface ITypography {
    desk: string
    mob: string
}

export type ThemeColorType = 'main' | 'secondary' | 'third' | 'dark' | 'grey' | 'lighter' | 'text' | 'addButtonDisabled' | 'addButtonHover' | 'addButtonActive' | 'completeButton' | 'completeButtonActive' | 'error'

declare module 'styled-components' {
    
    export interface DefaultTheme {
        borderRadius: string
        borderRadiusButton: string
        colors: {[key in ThemeColorType]: string}
        fontSizes: {
            small: ITypography
            h1: ITypography
            h4: ITypography
            p: ITypography
        }

        mediaQueries: {
            breakpointXs: string,
            breakpointSm: string,
            breakpointMd: string,
            breakpointLg: string,
            breakpointXl: string
        }
    }
}

const theme: DefaultTheme = {
    borderRadius: '12px',
    borderRadiusButton: '8px',
    colors: {
        main: '#ffffff',
        secondary: '#202A43',
        third: '#212944',
        dark: '#141B2C',
        grey: '#425277',
        lighter: '#A9C8E8',
        text: '#CBDDF0',
        addButtonDisabled: '#212944',
        addButtonHover: '#74ACE2',
        addButtonActive: '#4788C7',
        completeButton: '#82C94E',
        completeButtonActive: '#98C578',
        error: '#CD5050'
    },
    fontSizes : {
        small: {desk: '12px', mob: '8px'},
        h1: { desk: '24px', mob: '24px'},
        h4: {desk: '16px', mob: '16px'},
        p: {desk: '16px', mob: '14px'}
    },
    mediaQueries: {
        breakpointXs: 'only screen and (max-width: 420px)',
        breakpointSm: 'only screen and (max-width: 576px)',
        breakpointMd: 'only screen and (max-width: 768px)',
        breakpointLg: 'only screen and (max-width: 1025px)',
        breakpointXl: 'only screen and (min-width: 1170px)'
    }
}

const Theme = ({children}: any) => {
    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default Theme;
