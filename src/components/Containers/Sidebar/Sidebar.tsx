import React, { useContext } from 'react'
import styled from 'styled-components';
import { GlobalContext } from '../../../hooks/context/Context';
import { NavItems } from '../../UI';

const Wrapper = styled('div')`
    background: ${({theme}) => theme.colors.dark};
    width: 330px;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0px;
    padding: 40px;
    float: left;

    @media ${({theme}) => theme.mediaQueries.breakpointLg} {
        width: 250px;
    }

    @media ${({theme}) => theme.mediaQueries.breakpointMd} {
        width: 100%;
        z-index: 1;
        top: auto;
        bottom: 0;
        height: auto;
        padding: 10px 0px;
        float: left;
    }
`;

const WrapperList = styled('ul')`
    width: 100%;
    list-style-type: none;
    padding: 0;
    margin: 160px 0px 0px 0px;
    float: left;

    @media ${({theme}) => theme.mediaQueries.breakpointMd} {
        margin: 0px;
        display: flex;
    }
`;



export default function Sidebar() {
    const context = useContext(GlobalContext)

    const { tasks } = context!.todo.state;
    const countTaskCompleted = tasks.filter(x => x.isCompleted === true).length

    const ITEMS = [
        {name: 'Dashboard', counter: `${countTaskCompleted}/${tasks.length}`},
        {name: 'Calendar'},
        {name: 'Teams'},
        {name: 'Charts'}
    ]

    return (
        <Wrapper>
            <WrapperList>
                {renderList()}
            </WrapperList>
        </Wrapper>
    )

    function renderList(){
        return ITEMS.map(x => {
            return (
                <NavItems
                    key={x.name}
                    icon={`${process.env.PUBLIC_URL}/media/${x.name.toLocaleLowerCase()}.svg`}
                    text={x.name}
                    counter={x.counter} />
                );
        });
    }
}
