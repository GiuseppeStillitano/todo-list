import React, { useContext, useState } from 'react'
import styled from 'styled-components';
import { GlobalContext } from '../../../hooks/context/Context';
import { ModalActionType } from '../../../hooks/reducers/modalReducer';
import { ToDoActionType } from '../../../hooks/reducers/todoReducer';
import { Task } from '../../../models/implementations/Task';
import { Button, Input, Text } from '../../UI'
import { Switch } from '../../UI/Switch/Switch';

const Wrapper = styled('div')`
    width: calc(100% - 40px);
    background: ${({theme}) => theme.colors.dark};
    border-radius: ${({theme}) => theme.borderRadius};
    padding: 20px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`;

const ContainerDesktop = styled('div')`
    display: contents;
    @media ${({theme}) => theme.mediaQueries.breakpointLg} {
        display: none;
    }
`;

const ContainerMobile = styled('div')`
    display: none;
    @media ${({theme}) => theme.mediaQueries.breakpointLg} {
        display: contents;
    }
`;

const CallToAction = styled('div')`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    cursor: pointer;

    ${Text} {
        font-weight: 500;
    }

    img {
        margin: 0px 10px;
    }
`;

export default function NewTask() {
    const context = useContext(GlobalContext);
    const [task, setTask] = useState(Task.createEmpty());

    return (
        <Wrapper>
            {renderDesktop()}
            {renderMobile()}
        </Wrapper>
    )

    function renderDesktop(){
        return (
            <ContainerDesktop>
                {rederInputNewTask()}
                {renderSwitch()}
                {renderButtonAdd()}
            </ContainerDesktop>
        )
    }

    function renderMobile(){
        return (
            <ContainerMobile>
                <CallToAction onClick={(e) => handleClickMobile(e)}>
                    <Text>New Task</Text>
                    <img
                        src={`${process.env.PUBLIC_URL}/media/icon/add.svg`}
                        alt={'icon add'} />
                </CallToAction>
            </ContainerMobile>
        )
    }

    function rederInputNewTask() {
        return (
            <Input
                value={task.content}
                onChange={(e) => handleChangeInput(e)}/>
        );
    }

    function renderSwitch() {
        return <Switch
            checked={task.isPriority}
            onChange={e => handleChangeSwitch(e)}/>;
    }

    function renderButtonAdd(){
        return (
            <Button
                styleType='add'
                disabled={task.content.length < 3}
                onClick={() => handleSaveTask()}
            >ADD</Button>
        );
    }

    function handleClickMobile(e: any){
        const { modal } = context!
        e.preventDefault();
        modal.modalDispatch({
            type: ModalActionType.RESET_MODAL_CONTENT
        });
        modal.modalDispatch({
            type: ModalActionType.TOGGLE_MODAL
        });
    }

    function handleChangeInput(e: React.ChangeEvent<HTMLInputElement>){
        e.preventDefault();
        try {
            task.content = e.currentTarget.value;
            setTask(task.clone());
        } catch (error) {
            console.log(error)
        }
    }

    function handleChangeSwitch(e: React.ChangeEvent<HTMLInputElement>) {
        try {
            task.isPriority = !task.isPriority;
            setTask(task.clone());
        } catch (error) {
            
        }
    }

    async function handleSaveTask() {
        const {state, todoDispatch} = context!.todo
        try {
            await todoDispatch({
                type: ToDoActionType.ADD_TASK,
                payload: state.tasks.concat(task)
            })
            setTask(Task.createEmpty())
        }
        catch(error) {
            console.log(error)
        }
    }
}
