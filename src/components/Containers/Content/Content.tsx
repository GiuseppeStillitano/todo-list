import React from 'react'
import styled from 'styled-components';

const Wrapper = styled('div')`
    width: 100%;
    background: ${({theme}) => theme.colors.secondary};
    width: calc(100% - 490px);
    min-height: 100vh;
    padding: 40px;
    position: absolute;
    right: 0;
    top: 0;

    @media ${({theme}) => theme.mediaQueries.breakpointLg} {
        width: calc(100% - 410px);
    }

    @media ${({theme}) => theme.mediaQueries.breakpointMd} {
        padding-top: 50px;
        width: calc(100% - 80px);
        position: relative;
    }
`;

const ContainerTasks = styled('div')`
    max-width: 800px;
    margin: 0 auto;
    
`

export const Content: React.FC = (props) => {
    return (
        <Wrapper>
            <ContainerTasks>
                {props.children}
            </ContainerTasks>
        </Wrapper>
    )
}
