import React from 'react';
import { Modal } from '../../UI';
import { Content } from '../Content/Content';
import Sidebar from '../Sidebar/Sidebar';

export const TemplatePages: React.FC = (props) => {
    return (
        <div>
            <Modal/>
            <Sidebar/>
            <Content>
                {props.children}
            </Content>
        </div>
    )
}
