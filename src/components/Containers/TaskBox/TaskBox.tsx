import React, { useContext } from 'react'
import styled, { css } from 'styled-components'
import { GlobalContext } from '../../../hooks/context/Context';
import { ModalActionType } from '../../../hooks/reducers/modalReducer';
import { ToDoActionType } from '../../../hooks/reducers/todoReducer';
import { Checkbox, CirclePriority, Text } from '../../UI'

interface TaskBoxProps {
    id: string
    isPriority: boolean
    isCompleted: boolean
    content: string
}
const IconTrash = styled('img')`
    position: absolute;
    right: 20px;
    opacity: 0;
    transform: scale(0%);
    transition: all 90ms ease-in-out;
`;

const WrapperTask = styled('div')<{isCompleted: boolean}>`
    padding: 10px 20px;
    background: ${({theme}) => theme.colors.dark};
    border-radius: ${({theme}) => theme.borderRadius};
    display: flex;
    align-items: center;
    position: relative;
    cursor: pointer;
    margin: 15px 0px;
    
    ${Text} {
        overflow: hidden;
        width: 78%;
        position: relative;
    }

    &:after {
            content: '';
            position: absolute;
            background-image: linear-gradient(90deg, rgba(20,27,44,0) 0%, #141B2C 100%);
            z-index: 1;
            width: 10px;
            height: 100%;
            right: 42px;
        }

    &:hover {
        ${IconTrash} {
            opacity: 1;
            transform: scale(100%);
        }
    }

    ${({isCompleted}) => isCompleted && css`
        ${Text} {
            color: ${({theme}) => theme.colors.grey};
            text-decoration: line-through;
        }
    `}

    .c-mob {
        display: none;
    }

    @media ${({theme}) => theme.mediaQueries.breakpointMd} {
        .c-mob {
            display: contents;
        }
        .c-desk {
            display: none;
        }

        &:hover {
            ${IconTrash} {
                display: none;
            }
        }
    }
`;

export const TaskBox: React.FC<TaskBoxProps> = (props) => {
    const context = useContext(GlobalContext)

    return (
        <WrapperTask isCompleted={props.isCompleted}>
            {renderCheckbox({isMobile: true})}
            {renderCheckbox({isMobile: false})}
            <CirclePriority isPriority={props.isPriority}/>
            <Text>{props.content}</Text>
            <IconTrash
                onClick={() => handleDeleteTask()}
                src={`${process.env.PUBLIC_URL}/media/icon/trash.svg`}/>
        </WrapperTask>
    )

    function renderCheckbox({isMobile}: {isMobile: boolean}) {
        return (
            <span className={isMobile ? 'c-mob' : 'c-desk'}>
                <Checkbox
                    checked={props.isCompleted}
                    onChange={() => {
                        isMobile ?
                            handleCheckBoxMobile() :
                            handleCheckBoxDesktop()
                    }}/>
            </span>
        )
    }

    function handleDeleteTask(){
        const { tasks } = context!.todo.state
        const updateTasks = tasks.filter(x => x.id !== props.id).map(x => x)
        context!.todo.todoDispatch({
            type: ToDoActionType.UPDATE_TASKS,
            payload: [...updateTasks]
        })
    }

    function handleCheckBoxDesktop(){
        const { tasks } = context!.todo.state
        const updateTask = tasks.filter(x => x.id === props.id)[0]
        updateTask.isCompleted = !updateTask.isCompleted
        context!.todo.todoDispatch({
            type: ToDoActionType.UPDATE_TASKS,
            payload: [...tasks]
        })
    }

    async function handleCheckBoxMobile(){
        const { modalDispatch } = context!.modal
        await modalDispatch({
            type: ModalActionType.RESET_MODAL_CONTENT,
        })
        await modalDispatch({
            type: ModalActionType.UPDATE_MODAL_CONTENT,
            payload: props
        })
    }
}
