import styled, { css } from "styled-components"

type StyledButtonProps = 'add' | 'complete'

export interface ButtonStyledProps {
    styleType: StyledButtonProps
}

export const styledFromType = (type: StyledButtonProps) => {
    if (type === 'add') return css`
        padding: 20px 30px;
        background: ${({theme}) => theme.colors.addButtonActive};
        border-radius: ${({theme}) => theme.borderRadiusButton};
        color: ${({theme}) => theme.colors.text};
        line-height: 0px;
        &:hover {
            background: ${({theme}) => theme.colors.addButtonHover};
        }
        &:disabled {
            background: ${({theme}) => theme.colors.addButtonDisabled};
        }
    `;

    else return css`
        padding: 15px 30px;
        background: ${({theme}) => theme.colors.completeButton};
        border-radius: ${({theme}) => theme.borderRadiusButton};
        color: #fff;
        &:hover {
            background: ${({theme}) => theme.colors.completeButtonActive};
        }
    `
}

export const Button = styled.button<ButtonStyledProps>`
    width: fit-content;
    transition: all 200ms ease-in-out;
    outline: none;
    box-shadow: none;
    border: none;
    cursor: pointer;
    ${({styleType}) => styleType && css `
        ${styledFromType(styleType)}
    `}
`