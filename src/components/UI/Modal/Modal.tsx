import React, { useContext, useState } from 'react'
import styled from 'styled-components'
import { GlobalContext } from '../../../hooks/context/Context';
import { ModalActionType } from '../../../hooks/reducers/modalReducer';
import { ToDoActionType } from '../../../hooks/reducers/todoReducer';
import { Task } from '../../../models/implementations/Task';
import { Button, Input, Switch, Text }  from '../../UI'
import { CirclePriority } from '../CirclePriority/CirclePriority';

interface ModalProps {
}

const WrapperModal = styled('div')`
    position: fixed;
    width: 100%;
    height: 100vh;
    top: 0;
    left: 0;
    background: #020713cb;
    z-index: 100;
    overflow-y: scroll;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const ContainerModal = styled('div')`
    width: 80%;
    background: ${({theme}) => theme.colors.dark};
    border-radius: ${({theme}) => theme.borderRadius};
    padding: 10px 20px;
`;

const HeaderModal = styled('div')`
    border-bottom: 1px solid #1F2942;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding-bottom: 8px;
    
    ${Text} {
        font-weight: 500;
        font-size: 16px;
    }
    img {
        transform: rotate(45deg)
    }
`;

const WrapperTitleHead = styled('span')`
    display: flex;
    align-items: center;
`;

const WrapperBodyModal = styled('div')`
    margin: 20px 0px;
    min-height: 100px;
    input {
        width: 100%;
    }
`;

const FooterModal = styled('div')`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    align-self: flex-end;
    padding-bottom: 10px;
`;

const IconTrash = styled('div')`
    width: 45px;
    height: 45px;
    background: ${({theme}) => theme.colors.third};
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: ${({theme}) => theme.borderRadiusButton};
    cursor: pointer;
`;

export const Modal: React.FC<ModalProps> = () => {
    const context = useContext(GlobalContext)!
    const [task, setTask] = useState(Task.createEmpty());

    return renderContainerModal();

    function renderContainerModal(){
        const { opened } = context.modal.state
        if (opened) return (
            <WrapperModal>
                <ContainerModal>
                    {renderHeader()}
                    {renderBodyModal()}
                    {renderFooterModal()}
                </ContainerModal>
            </WrapperModal>
        );
        else return null
    }

    function renderHeader(){
        return (
            <HeaderModal>
                {renderTitleHead()}
                <img
                    onClick={(e) => handleToggleModel(e)}
                    src={`${process.env.PUBLIC_URL}/media/icon/add.svg`}
                    alt="close icon" />
            </HeaderModal>
        );
    }

    function renderTitleHead(){
        const { content } = context.modal.state;
        if (!content) return (
            <Text>New Task</Text>
        );
        else return (
            <WrapperTitleHead>
                <CirclePriority isPriority={content.isPriority}/>
                <Text>Task</Text>
            </WrapperTitleHead>
        );
    }

    function renderBodyModal(){
        const { content } = context.modal.state;
        return (
            <WrapperBodyModal>
                {content ?
                    renderContentTask() :
                    renderInputTask()
                }
            </WrapperBodyModal>
        )
    }

    function renderContentTask() {
        const { content } = context.modal.state.content!
        return (
            <Text>{content}</Text>
        )
    }

    function renderInputTask() {
        return (
            <Input
                value={task.content}
                onChange={(e) => handleChangeInput(e)}/>
        );
    }

    function renderFooterModal(){
        const { content } = context.modal.state;
        return (
            <FooterModal>
                { content ?
                    renderControlsEditTask() :
                    renderControlsNewTask()
                }
            </FooterModal>
        );
    }

    function renderControlsNewTask(){
        return <>
            <Switch
                checked={task.isPriority}
                onChange={(e: any) => handleChangeSwitch(e)}/>
            <Button
                styleType='add'
                disabled={task.content.length < 3}
                onClick={() => handleSaveTask()}
            >ADD</Button>
        </>;
    }

    function renderControlsEditTask(){
        return(
            <>
                <IconTrash onClick={() => handleDeleteTask()}>
                    <img
                        src={`${process.env.PUBLIC_URL}/media/icon/trash.svg`}
                        alt="icon trash" />
                </IconTrash>
                <Button
                    styleType={'complete'}
                    onClick={() => handleCompletedTask()}>
                    COMPLETE
                </Button>
            </>
        );
    }

    function handleToggleModel(e: any){
        e.preventDefault();
        const { modal } = context;
        modal.modalDispatch({
            type: ModalActionType.TOGGLE_MODAL
        })
    }

    function handleChangeInput(e: React.ChangeEvent<HTMLInputElement>){
        e.preventDefault();
        try {
            task.content = e.currentTarget.value;
            setTask(task.clone());
        } catch (error) {
            console.log(error)
        }
    }

    function handleChangeSwitch(e: React.ChangeEvent<HTMLInputElement>) {
        try {
            task.isPriority = !task.isPriority;
            setTask(task.clone());
        } catch (error) {
            
        }
    }

    async function handleSaveTask() {
        const {todo, modal} = context!
        try {
            await todo.todoDispatch({
                type: ToDoActionType.ADD_TASK,
                payload: todo.state.tasks.concat(task)
            })
            await modal.modalDispatch({
                type: ModalActionType.TOGGLE_MODAL,
            })
            setTask(Task.createEmpty())
        }
        catch(error) {
            console.log(error)
        }
    }

    async function handleCompletedTask() {
        const {todo, modal} = context!
        try {
            const updateTask = todo.state.tasks.filter(x => x.id === modal.state.content?.id)[0];
            updateTask.isCompleted = !updateTask.isCompleted;
            await context!.todo.todoDispatch({
                type: ToDoActionType.UPDATE_TASKS,
                payload: [...todo.state.tasks]
            })
            await modal.modalDispatch({
                type: ModalActionType.TOGGLE_MODAL,
            })
        } catch (error) {
            console.log(error)
        }
    }

    async function handleDeleteTask(){
        const {todo, modal} = context!
        const updateTasks = todo.state.tasks.filter(x => x.id !== modal.state.content?.id).map(x => x)
        try {
            await context!.todo.todoDispatch({
                type: ToDoActionType.UPDATE_TASKS,
                payload: [...updateTasks]
            })
            await modal.modalDispatch({
                type: ModalActionType.TOGGLE_MODAL,
            })
        } catch (error) {
            console.log(error)
        }
    }

}
