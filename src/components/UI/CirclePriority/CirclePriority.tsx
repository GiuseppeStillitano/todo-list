import styled, { css } from "styled-components";

interface CirclePriorityProps {
    isPriority: boolean
}

export const CirclePriority = styled('div')<CirclePriorityProps>`
    width: 10px;
    height: 10px;
    border-radius: 10px;
    margin: 0px 10px 0px 0px;
    background: ${({theme}) => theme.colors.grey};
    
    ${({isPriority}) => isPriority && css`
        background: ${({theme}) => theme.colors.error};
    `}
`;