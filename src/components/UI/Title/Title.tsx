import styled, {css} from 'styled-components';

interface TitleStyledProps {
    marginTop?: string
    hideMobile?: boolean
}

const BASIC_STYLED = css<TitleStyledProps>`

    ${({marginTop}) => marginTop && css`
        margin-top: ${marginTop};
    `}

    ${({hideMobile}) => hideMobile && css`
        @media ${({theme}) => theme.mediaQueries.breakpointMd} {
            display: none;
        }
    `}
`

export const Title = styled('h1')<TitleStyledProps>`
    ${BASIC_STYLED}
    color: ${({ theme: { colors } }) => colors.main};
    font-weight: 700;
    font-size: ${({theme}) => `calc(${theme.fontSizes.h1.mob} +
        (${parseInt(theme.fontSizes.h1.desk, 10)} - ${parseInt(theme.fontSizes.h1.mob)}) *
        ((100vw - 300px) / (1200 - 300)))
    `};
`;

export const TitleH4 = styled('h4')<TitleStyledProps>`
    ${BASIC_STYLED}
    color: ${({ theme: { colors } }) => colors.text};
    font-weight: 500;
    font-size: ${({theme}) => `calc(${theme.fontSizes.h4.mob} +
        (${parseInt(theme.fontSizes.h4.desk, 10)} - ${parseInt(theme.fontSizes.h4.mob)}) *
        ((100vw - 300px) / (1200 - 300)))
    `};
`;