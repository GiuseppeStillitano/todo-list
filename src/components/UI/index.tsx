import { Switch } from "./Switch/Switch";
import { Button } from "./Button/Button";
import { Checkbox } from "./Checkbox/Checkbox";
import { Counter } from "./Counter/Counter";
import { Input } from "./Input/Input";
import { Modal } from "./Modal/Modal";
import { NavItems } from "./NavItems/NavItems";
import { Title, TitleH4 } from "./Title/Title";
import { Text, SmallText} from "./Typography/Typography";
import { CirclePriority } from "./CirclePriority/CirclePriority";

export {
    Title,
    TitleH4,
    Text,
    SmallText,
    Button,
    NavItems,
    Switch,
    Input,
    Counter,
    Checkbox,
    Modal,
    CirclePriority
}