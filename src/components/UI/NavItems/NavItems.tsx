import React from 'react'
import styled from 'styled-components'
import { Counter, Text } from '../'

export interface NavItemsProps {
    icon: string
    text: string
    counter?: string
}

const Wrapper =  styled('li')`
    max-width: 100%;
    padding: 13px 20px;
    border-radius: ${({theme}) => theme.borderRadius};
    display: flex;
    flex-direction: row;
    align-items: center;
    margin: 20px 0px;
    cursor: pointer;
    position: relative;

    &:first-child, &:hover {
        background: ${({theme}) => theme.colors.addButtonDisabled};
    }

    img {
        margin-right: 15px;
    }

    ${Text} {
        line-height: 0%;
        font-weight: 700;
        min-width: 120px;
    }

    @media ${({theme}) => theme.mediaQueries.breakpointMd} {
        flex-direction: column;
        align-items: center;
        text-align: center;
        padding: 0;
        margin: 5px 0px;
        width: 25%;

        img {
            margin: 0;
        }

        &:first-child, &:hover {
            background: none
        }

        ${Text} {
            clear: both;
            font-weight: 400;
            margin-top: 15px;
            text-align: center;
            line-height: 120%;
            min-width: auto;
        }

        ${Counter} {
            display: none;
        }
    }
`;

export const NavItems: React.FC<NavItemsProps> = (props) => {
    return (
        <Wrapper>
            <img src={props.icon} alt="nav icons" />
            <Text>{props.text}</Text>
            {renderCounter()}
        </Wrapper>
    )

    function renderCounter(){
        if (props.counter) return (
            <Counter positionAbsolute right='20px'>
                {props.counter}
            </Counter>
        );
        else return null;
    }
}
