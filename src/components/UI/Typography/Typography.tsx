import styled, {css} from 'styled-components';

interface TipographyStyledProps {
    color?: string
    size?: string
    small?: boolean
}

const BASIC_STYLED = css<TipographyStyledProps>`
    color: ${({theme}) => theme.colors.text};
    line-height: 130%;

    ${({color}) => color && css`
        color: ${color};
    `};

    ${({size}) => size && css`
        font-size: ${size}!important;
    `};

    ${({small}) => small && css`
        font-size: ${({theme}) => `calc(${theme.fontSizes.small.mob} +
        (${parseInt(theme.fontSizes.small.desk, 10)} - ${parseInt(theme.fontSizes.small.mob)}) *
        ((100vw - 300px) / (1200 - 300)))!important
    `};
    `};
`
export const Text = styled('p')<TipographyStyledProps>`
    ${BASIC_STYLED};
    font-size: ${({theme}) => `calc(${theme.fontSizes.p.mob} +
        (${parseInt(theme.fontSizes.p.desk, 10)} - ${parseInt(theme.fontSizes.p.mob)}) *
        ((100vw - 300px) / (1200 - 300)))
    `};
`;
export const SmallText = styled('span')<TipographyStyledProps>`
    ${BASIC_STYLED};
    font-size: ${({theme}) => `calc(${theme.fontSizes.small.mob} +
        (${parseInt(theme.fontSizes.small.desk, 10)} - ${parseInt(theme.fontSizes.small.mob)}) *
        ((100vw - 300px) / (1200 - 300)))
    `}
`;