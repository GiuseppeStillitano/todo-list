import styled, { css } from 'styled-components';

export interface CounterProps {
    secondary?: boolean
    positionAbsolute?: boolean
    top?: string
    left?: string
    right?: string
    bottom?: string
    marginLeft?: string
}

export const Counter = styled('span')<CounterProps>`
    display: inline-block;
    width: fit-content;
    background: #fff;
    padding: 10px 8px;
    border-radius: 15px;
    line-height: 0px;
    font-size: 12px;
    color: ${({theme}) => theme.colors.third};

    ${({secondary}) => secondary && css`
        background: ${({theme}) => theme.colors.text};
    `}

    ${({positionAbsolute}) => positionAbsolute && css`
        position: absolute;
    `}

    ${({top}) => top && css`
        top: ${top};
    `}

    ${({left}) => left && css`
        left: ${left};
    `}

    ${({right}) => right && css`
        right: ${right};
    `}

    ${({bottom}) => bottom && css`
        bottom: ${bottom};
    `}

    ${({marginLeft}) => marginLeft && css`
        margin-left: ${marginLeft};
    `}
`;