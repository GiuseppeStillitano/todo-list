import React from 'react'
import styled from 'styled-components'

export interface SwitchProps {
    checked?: boolean
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const Label = styled('label')`
    position: relative;
    display: flex;
    align-items: center;

    .text {
        font-size: 12px;
        color: ${({theme}) => theme.colors.text};
        margin: 0px 40px 0px 10px
    }
`;

const Slider = styled('span')`
    width: 60px;
    height: 32px;
    float: left;
    border-radius: 32px;
    z-index: 1;
    background: ${({theme}) => theme.colors.third};

    &:after {
        content: '';
        position: absolute;
        width: 26px;
        height: 26px;
        top: 3px;
        left: 3px;
        background: ${({theme}) => theme.colors.grey};
        border-radius: 26px;
        transition: all 200ms ease-in-out;
        pointer-events: none;
    }
`;

const WrapperSlider = styled('div')`
    position: relative;

    input {
        position: absolute;
        right: 0px;
        width: 100%;
        height: 100%;
        z-index: 0;
        margin: 0;
        opacity: 0;
    }

    input:checked + ${Slider}:after {
        transform: translateX(28px);
        background: ${({theme}) => theme.colors.text};
    }
`;

export const Switch: React.FC<SwitchProps> = (props) => {
    return (
        <Label>
            <span className='text'>High priority</span>
            {renderSlider()}
        </Label>
    )

    function renderSlider(){
        return (
            <WrapperSlider>
                <input
                    type='checkbox'
                    onChange={(e) => props.onChange(e)}
                    checked={props.checked}/>
                <Slider/>
            </WrapperSlider>
        )
    }
}
