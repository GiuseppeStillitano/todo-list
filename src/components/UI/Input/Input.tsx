import React from 'react'
import styled from 'styled-components'

interface InputProps {
    value: string
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const InputStyled = styled('input')`
    background: none;
    outline: none;
    box-shadow: none;
    border: none;
    font-size: 16px;
    color: ${({theme}) => theme.colors.lighter};
    width: 50%;

    &::placeholder {
        color: ${({theme}) => theme.colors.grey};
    }
`;

export const Input: React.FC<InputProps> = (props) => {
    return (
        <InputStyled
            type='text'
            placeholder='Insert task title...'
            onChange={(e) => props.onChange ? props.onChange(e) : null}
            value={props.value}/>
    )
}
