import styled from "styled-components"

interface CheckboxProps {
    checked?: boolean
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}


const WrapperCheck = styled('div')`
    position: relative;
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 20px;
    margin-right: 20px;
`;

const Check = styled('span')`
    width: 20px;
    height: 20px;
    border-radius: 4px;
    border: 1px solid #fff;
    position: relative;
    pointer-events: none;

    &:after {
        content: url('./media/icon-check.svg');
        position: absolute;
        border-radius: 4px;
        display: flex;
        align-items: center;
        justify-content: center;
        width: 22px;
        height: 22px;
        margin-top: -1px;
        margin-left: -1px;
        background: ${({theme}) => theme.colors.completeButton};
        transition: all 100ms ease-in-out;
        transform: scale(0%);
        pointer-events: none;
    }
`;

const Label = styled('label')`
    position: relative;
    input {
        position: absolute;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
        cursor: pointer;

        &:checked + ${Check}:after {
            transform: scale(100%);
        }

        &:hover + ${Check}:after {
            transform: scale(100%);
        }
    }
`;


export const Checkbox: React.FC<CheckboxProps> = (props) => {
    return (
        <Label>
            {renderSlider()}
        </Label>
    )

    function renderSlider(){
        return (
            <WrapperCheck>
                <input
                    type='checkbox'
                    onChange={(e) => props.onChange(e)}
                    checked={props.checked}/>
                <Check/>
            </WrapperCheck>
        )
    }
}
