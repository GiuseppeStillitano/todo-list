import React from 'react';
import styled from 'styled-components';
import { Title } from '../UI';

const HeaderStyled = styled.header`
    position: fixed;
    top: 0;
    width: calc(100% - 80px);
    z-index: 100;
    padding: 40px;

    @media ${({theme}) => theme.mediaQueries.breakpointMd} {
        position: relative;
        float: left;
    }
`;

export default function Header() {
    return (
        <HeaderStyled>
            <Title>MyTrack</Title>
        </HeaderStyled>
    )
}
