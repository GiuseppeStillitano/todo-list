export interface TaskDTO {
    id: string
    content: string
    isPriority: boolean
    isCompleted: boolean
}