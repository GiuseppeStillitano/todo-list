import { TaskDTO } from "../dtos/TaskDTO";

export interface TaskValues {
    id: string
    content: string
    isPriority: boolean
    isCompleted: boolean
}

export class Task {
    private readonly values: TaskValues;

    private constructor(values: TaskValues) {
        this.values = values;
    }

    public createFromDTO(dto: TaskDTO) {
        return new Task({
            id: dto.id,
            content: dto.content,
            isPriority: dto.isPriority,
            isCompleted: dto.isCompleted
        })
    }

    public clone(){
        const newValues = { ...this.values };
        return new Task(newValues);
    }

    public static createEmpty() {
        return new Task({
            id:  Math.random().toString(36).substr(2, 9),
            content: '',
            isPriority: false,
            isCompleted: false
        })
    }

    public get id(){
        return this.values.id;
    }

    public get content(){
        return this.values.content;
    }

    public set content(content: string){
        this.values.content = content;
    }

    public get isPriority(){
        return this.values.isPriority;
    }

    public set isPriority(isPriority: boolean){
        this.values.isPriority = isPriority;
    }

    public get isCompleted(){
        return this.values.isCompleted;
    }

    public set isCompleted(isCompleted: boolean){
        this.values.isCompleted = isCompleted;
    }
}