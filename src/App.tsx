import React, { useMemo, useReducer } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './components/navigationals/Header';
import { GlobalContext } from './hooks/context/Context';
import ModalReducer, { initialStateModal } from './hooks/reducers/modalReducer';
import ToDoReducer, { initialStateToDo } from './hooks/reducers/todoReducer';
import { PageDashboard } from './Pages';

function App() {
	const [todoState, todoDispatch] = useReducer(ToDoReducer, initialStateToDo);
	const [modalState, modalDispatch] = useReducer(ModalReducer, initialStateModal);

	const value = useMemo(() => ({
		todo: { state: todoState, todoDispatch},
		modal: { state: modalState, modalDispatch}
	}), [todoState, modalState]);

	return (
		<GlobalContext.Provider value={value}>
			<BrowserRouter>
				<Header/>
				<Switch>
					<Route path='/' exact component={PageDashboard}/>
				</Switch>
			</BrowserRouter>
		</GlobalContext.Provider>
	);
}

export default App;
