import { Task } from "../../models/implementations/Task";

export enum ModalActionType {
    RESET_MODAL_CONTENT = 'RESET_MODAL_CONTENT',
    TOGGLE_MODAL = 'TOGGLE_MODAL',
    UPDATE_MODAL_CONTENT = 'UPDATE_MODAL_CONTENT',
}

export interface IModalReducer {
    type: ModalActionType
    payload?: any
}

export interface ModalState {
    opened: boolean
    content?: Task | null
}

export const initialStateModal: ModalState = {
    opened: false,
    content: null
}

const ModalReducer: React.Reducer<ModalState, IModalReducer> = (state, action) => {
    switch (action.type) {
        case ModalActionType.RESET_MODAL_CONTENT:
            return { 
                ...state,
                content: null
            }

        case ModalActionType.UPDATE_MODAL_CONTENT:
            return { 
                ...state,
                content: action.payload,
                opened: !state.opened
            }

        case ModalActionType.TOGGLE_MODAL:
            return { 
                ...state,
                opened: !state.opened
            }

        default:
            return state;
    }
}

export default ModalReducer;