import { Task } from "../../models/implementations/Task";

export enum ToDoActionType {
    RESET_TODO_LIST = 'RESET_TODO_LIST',
    ADD_TASK = 'ADD_TASK',
    UPDATE_TASKS = 'UPDATE_TASKS'
}

export interface IToDoReducer {
    type: ToDoActionType
    payload?: any
}

export interface ToDoState {
    tasks: Task[]
}

export const initialStateToDo: ToDoState = {
    tasks: []
}

const ToDoReducer: React.Reducer<ToDoState, IToDoReducer> = (state, action) => {
    switch (action.type) {
        case ToDoActionType.RESET_TODO_LIST:
            return { tasks: null }

        case ToDoActionType.ADD_TASK:
            return { tasks: action.payload }

        case ToDoActionType.UPDATE_TASKS:
            return { tasks: action.payload }

        default:
            return state;
    }
}

export default ToDoReducer;