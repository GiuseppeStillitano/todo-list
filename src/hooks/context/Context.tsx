import { createContext, Dispatch } from "react";
import { IModalReducer, ModalState } from "../reducers/modalReducer";
import { IToDoReducer, ToDoState } from "../reducers/todoReducer";

type StateContextProps = {
    todo: { state: ToDoState, todoDispatch: Dispatch<IToDoReducer> }
    modal: { state: ModalState, modalDispatch: Dispatch<IModalReducer>}
}

export const GlobalContext = createContext<StateContextProps | undefined>(undefined);